//JaHOVA OS Instance
var os = com.jahova.os.Instance();

//Utility Functions, Classes and Objects
var playstylelabs = (function(module){
    //PRIVATE MODULE PROPERTIES
    var pInstance = null;
    
    //CONSTANTS
    var NAME = "PlayStyleLabs";
    var VERSION = "0.1";
    var PATH = "scripts/psl.js";
    var BROWSER = null;
     
    var os = module;                                   
    //INTERFACE TO BIOLUCID MODULE
    var constructor = function(){

        //PRIVATE OBJECTS, PROPERTIES AND CLASS DEFINITIONS
        //

        //Animation Classes
        var CSprite = function(sName){
            var self = this;
            this.name = sName;
            this.graphic = os.resschmgr.Create.HTMLElement("div");
            this.graphic.html().id = sName;
            this.graphic.html().style.position = "absolute";
            this.Position ={
                x: 0,
                y: 0,
                z: 0
            }
            this.Rotation ={
                x: 0,
                y: 0,
                z: 0
            }
            this.Move = {
                Up: function(iDist){
                    self.Position.y -= iDist;
                    self.graphic.html().style.top = (self.Position.y).toFixed(0) + "px";
                },
                Down: function(iDist){
                    self.Position.y += iDist;
                    self.graphic.html().style.top = (self.Position.y).toFixed(0) + "px";
                },
                Left: function(iDist){
                    self.Position.x -= iDist;
                    self.graphic.html().style.left = (self.Position.x).toFixed(0) + "px";
                },
                Right: function(iDist){
                    self.Position.x += iDist;
                    self.graphic.html().style.left = (self.Position.x).toFixed(0) + "px";
                },
                ToPosition: function(iX, iY){
                    self.Position.x = iX;
                    self.Position.y = iY;
                    
                    if(self.graphic.html().style.webkitTransform != undefined)
                        self.graphic.html().style.webkitTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
                    else
                        self.graphic.html().style.MozTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
                    //self.graphic.html().style.left = (self.Position.x).toFixed(0) + "px";
                    //self.graphic.html().style.top  = (self.Position.y).toFixed(0) + "px";
                }
            }
            
            this.Rotate = function(deg){
                self.graphic.html().style.webkitTransform = "rotate("+ deg +"deg)";
            }
            
            this.Spin = {
                X: function(deg){
                    
                },
                Y: function(deg){
                    
                }
            }
            
            
            this.Animation = {
                list: os.resschmgr.Create.Map(),
                Current: {
                    animation: null,
                    frames: 0,
                    index: 0
                },
                loop: false,
                lastUpdate: 0,
                NextFrame: function(){//Transtion to next animation frame
                    self.Animation.lastUpdate = pInstance.Get.Time();
                    
                    self.Animation.Current.index = self.Animation.Current.index == self.Animation.Current.animation.numOfFrames - 1 ? 0  : self.Animation.Current.index + 1;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[self.Animation.Current.index];

                    self.graphic.html().style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
                },
                Update: function(){   //Transition to Next Frame, if ready
                    
                },
                Start: function(){    //Auto Animate
                    
                },
                Pause: function(){    //Pause Animation 
                    
                },
                Reset: function(){    //Reset Animation to First Frame
                    self.Animation.lastUpdate = new Date().getDate();
                
                    self.Animation.Current.index = 0;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[0];
                },
                Load: function(oAnimation){     //Load New Animation into Sprite Object
                    self.Animation.list.put(oAnimation.name, oAnimation);
                },
                SetActive: function(sName){//Set Animation
                    //Remove Current Animation Class
                    if(self.Animation.Current.animation)
                        self.graphic.Remove.Class(self.Animation.Current.animation.sheet.GetClass() + " ");
                    
                    //Set new animation object
                    self.Animation.Current.animation = self.Animation.list.get(sName);
                    
                    //Add animation class to html
                    self.graphic.Class.Add(self.Animation.Current.animation.sheet.GetClass());
                    
                    //Update frame on graphic
                    self.Animation.SetFrame(0);
                },
                SetFrame: function(iFrameNumber){ //Set To Specific Frame in Current Animation
                    self.Animation.lastUpdate = new Date().getDate();
                
                    self.Animation.Current.index = iFrameNumber;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[iFrameNumber];

                    self.graphic.html().style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
                }
                
            }
        }
    
        var CSpriteSheet = function(sName){
            var self = this;
            this.width = 0;
            this.height = 0;
            this.cellWidth = 0;
            this.cellHeight = 0;
            this.rows = 0;
            this.columns = 0;
            this.path = "";
            this.id = 0;
            this.name = sName;
            var className = ""
            this.BuildClass = function(sClassName){
                className = sClassName ? sClassName : self.name;
    
                var style = "position: absolute; ";
                style += "overflow: hidden;";
                style += "background: url('" + this.path + "'); ";
                style += "width: " + this.cellWidth +"px;";
                style += "height: " + this.cellHeight + "px;";
                
                pInstance.Create.CSSClass("."+className, style);
            }
            this.GetClass = function(){
                return className;
            }
        }
        
        var CAnimation = function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed){
            var self = this;
            this.name = sName;
            this.sheet = oSpriteSheet;
            this.numOfFrames = iNumOfFrames;
            this.StartFrame = null;
            this.StopFrame  = null;
            this.frames = [];
            this.speed =  speed ? speed : 0;
            //Set Stop Column, Row and index
            
            //Populate frames array
            
             {
                var _frame = 0;
                
                //need to have initial starting point, will be reset to 0 afterwards
                var _iStartColumn = iStartColumn;
                
                // runs through rows (y) and columns (x) and adds them to the frames array
                for(var y = iStartRow; y < oSpriteSheet.rows && _frame < iNumOfFrames; y++){
                    for (var x = _iStartColumn; x < oSpriteSheet.columns && _frame < iNumOfFrames; x++,_frame++){
                        var fr = new CAnimationFrame(x*oSpriteSheet.cellWidth,y*oSpriteSheet.cellHeight,oSpriteSheet.cellWidth,oSpriteSheet.cellHeight);
                        fr.frame = _frame;
                        fr.row = y;
                        fr.column = x;
                        self.frames.push(fr);
                    }
                    // resetting starting column (x) to 0, so it goes through all following columns
                    _iStartColumn = 0;

                }
            }
        }
    
        var CAnimationFrame = function(iX, iY, iWidth, iHeight){
            this.x = iX;
            this.y = iY;
            this.width = iWidth;
            this.height = iHeight;
            this.frame = 0;         //Sprite Sheet Frame Number
            this.row = 0;
            this.column = 0;
        }
        
        
        //  Finite State Machine
        var CFiniteStateMachine = function(cEntity){
            var owner = cEntity;
            var _CurrentState = new CState();
            var self = this;
            var _Enter = function(oMessage){
                _CurrentState.Enter(owner, oMessage);
            }
            
            var _Exit = function(oMessage){
                _CurrentState.Exit(owner, oMessage);
            }
            
            var _Execute = function(oMessage){
                _CurrentState.Execute(owner, oMessage);
            }
            
            //Control Methods
            this.Transition = function(sName, oMessage){
                //Call Exit for Current State
                _Exit(oMessage);
                
                //Get New State to Transition Too
                _CurrentState = _States.get(sName);
                
                //Call New States Setup and Exectue Methods
                _Enter(oMessage);
                _Execute(oMessage);
            }
            this.SetState = function(sName){
                _CurrentState = _States.get(sName);
            }
            this.Update = function(oMessage){
                _Execute(oMessage);
            }
            this.GetState = function(){
                return _CurrentState.GetName();
            }
        }
        
        // States for FSM
        var CState = function(sName){
            var _name = sName;
            var self = this;
            
            this.Enter = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.Exit = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.Execute = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.GetName = function(){
                return _name;
            }

        }
        //Holds all states created
        // key: name, value: CState
        var _States = os.resschmgr.Create.Map();
        var SpriteSheets = os.resschmgr.Create.Map();
        var Animations = os.resschmgr.Create.Map();
        var Sprites = os.resschmgr.Create.Map();
        
        
        //PUBLIC OBJECTS/METHODS
        return{
            Get: {
                Sprite: function(sName){
                    return Sprites.get(sName);
                },
                SpriteSheet: function(sName){
                    return SpriteSheets.get(sName)
                },
                Animation: function(sName){
                    return Animations.get(sName);
                },
                Time: function(){
                    return Date.now();
                },
                State: function(sName){
                    return _States.get(sName);  
                },
                Name: function(){
                    return NAME;
                },
                Version: function(){
                    return VERSION;
                },
                Path: function(){
                    return PATH;
                }
            },
            Remove: {
                Sprite: function(sName){
                    return Sprites.drop(sName);
                },
                SpriteSheet: function(sName){
                    return SpriteSheets.drop(sName)
                },
                Animation: function(sName){
                    return Animations.drop(sName);
                }
            },
            
            Create: {
                FSM: function(cEntity){
                    return new CFiniteStateMachine(cEntity);
                    
                },
                State: function(sName){
                    var state = new CState(sName);
                    _States.put(sName, state);
                    return state;
                },
                Sprite: function(sName){
                    var sprite = new CSprite(sName);
                    Sprites.put(sName, sprite);
                    
                    return  sprite;
                },
                SpriteSheet: function(sName){
                    var sheet = new CSpriteSheet(sName);
                    SpriteSheets.put(sName, sheet);
                    
                    return sheet;   
                },
                Animation: function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed){
                    var ani = new CAnimation(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed);
                    Animations.put(sName, ani);
                    
                    return ani;
                },
                AnimationFrame: function(iX, iY, iWidth, iHeight){
                    return new CAnimationFrame(iX, iY, iWidth, iHeight);
                },
                CSSClass: function(sClass, sStyle) {
                    if (!document.styleSheets) {
                        return;
                    }
                
                    if (document.getElementsByTagName("head").length == 0) {
                        return;
                    }
                
                    var stylesheet;
                    var mediaType;
                    if (document.styleSheets.length > 0) {
                        for (i = 0; i < document.styleSheets.length; i++) {
                            if (document.styleSheets[i].disabled) {
                                continue;
                            }
                            var media = document.styleSheets[i].media;
                            mediaType = typeof media;
                
                            if (mediaType == "string") {
                                if (media == "" || (media.indexOf("screen") != -1)) {
                                    styleSheet = document.styleSheets[i];
                                }
                            } else if (mediaType == "object") {
                                if (media.mediaText == "" || (media.mediaText.indexOf("screen") != -1)) {
                                    styleSheet = document.styleSheets[i];
                                }
                            }
                
                            if (typeof styleSheet != "undefined") {
                                break;
                            }
                        }
                    }
                
                    if (typeof styleSheet == "undefined") {
                        var styleSheetElement = document.createElement("style");
                        styleSheetElement.type = "text/css";
                
                        document.getElementsByTagName("head")[0].appendChild(styleSheetElement);
                
                        for (i = 0; i < document.styleSheets.length; i++) {
                            if (document.styleSheets[i].disabled) {
                                continue;
                            }
                            styleSheet = document.styleSheets[i];
                        }
                
                        var media = styleSheet.media;
                        mediaType = typeof media;
                    }
                
                    if (mediaType == "string") {
                        for (i = 0; i < styleSheet.rules.length; i++) {
                            if (styleSheet.rules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                                styleSheet.rules[i].style.cssText = sStyle;
                                return;
                            }
                        }
                
                        styleSheet.addRule(sClass, sStyle);
                    }
                    else if (mediaType == "object") {
                        for (i = 0; i < styleSheet.cssRules.length; i++) {
                            if (styleSheet.cssRules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                                styleSheet.cssRules[i].style.cssText = sStyle;
                                return;
                            }
                        }
                
                        styleSheet.insertRule(sClass + "{" + sStyle + "}", 0);
                    }
                }
                
            }
                
        }
    }
    return {
        //Instantiates Objects and Returns
        //  Insures a single object
        Instance: function()
        {
            if(!pInstance)
            {
                //Instantiate if pInstance does not exist
                pInstance = constructor();
            }
            
            return pInstance;
        }
    }
})(os);



//
//  Entry Method Called from OS
//
Application = function(){
    //Get pionter to JaHOVA OS
    os = com.jahova.os.Instance();
    
    //Connect HTML Objects to JavaScript
    //App.Load();
    
    //App Initializtion
    App.Init();
    
}

//
//  Application Object Definition
//
var App = {
    Load: function(){
        App.HTML.container = document.getElementById('app');
    
        App.HTML.Main.container = document.getElementById('main');
        App.HTML.Main.window = document.getElementById('main-window');
        App.HTML.Main.footer = document.getElementById('main-footer');
        
        App.HTML.Thumnails.container = document.getElementById('thumbnail');
        App.HTML.Thumnails.window = document.getElementById('thumbnail-window');
        App.HTML.Thumnails.footer = document.getElementById('thumbnail-footer');
        
        App.HTML.Properties.container = document.getElementById('properties');
        App.HTML.Properties.header = document.getElementById('properties-header');
        App.HTML.Properties.window = document.getElementById('properties-window');
        App.HTML.Properties.dropDown = document.getElementById("dropdown");
        App.HTML.Properties.dropDown.onchange = function(e){
            var win = os.windows.WindowsManager.Windows.get("jahova.window.id." + e.currentTarget.value);
            win.MakeActive();
            if(App.Events.onDropdown){App.Events.onDropdown(win);};
        }
    },
    Display: {
        State: 'windowed',
        Toggle: function(){
            App.Display.State == "windowed" ? App.Display.Fullscreen() : App.Display.Windowed();
        },
        Fullscreen: function(){
            App.Display.State = "fullscreen";
            
            //Scroll Screen to top left, to hide any overflow
            scrollTo(0,0);
            
            App.HTML.Thumnails.container.style.display = "none";
            
            App.HTML.Properties.container.style.display = "none";
            
            //var win = document.getElementById("main-window");
            App.HTML.Main.window.setAttribute("class", "fullscreen");
            App.HTML.Main.window.style.height = window.innerHeight + "px";
            
            document.body.style.overflow = "hidden";
            
            //Hide Debug Bar, to make sure its not causing any problems
            os.debugbar.Disable();
            
            if(App.Events.onFullscreen){App.Events.onFullscreen()}
        },
        Windowed: function(){
            App.Display.State = "windowed";
            
            App.HTML.Thumnails.container.style.display = "";
            
            App.HTML.Properties.container.style.display = "";
            
            App.HTML.Main.window.setAttribute("class", "main-window border-radius-top-10px");
            App.HTML.Main.window.style.height = "";
            
            document.body.style.overflow = "";
            
            //Hide Debug Bar, to make sure its not causing any problems
            os.debugbar.Enable();
            
            if(App.Events.onWindowed){App.Events.onWindowed()}
        },
        EnableButton: function(){
            App.HTML.Main.window.innerHTML =  '<div id="expand-button" class="expand-button-min" onclick="App.Display.Toggle()"></div>'
        }
    },
    HTML: {
        container: null,
        Main: {
            container: null,
            window: null,
            footer: null
        },
        Properties: {
            container: null,
            header: null,
            window: null,
            dropDown: null
        },
        Thumnails: {
            container: null,
            window: null,
            footer: null
        }
    },
    FileIO: {
        Add: {
            FileSelector:{
                Main: function(label){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    App.HTML.Main.footer.innerHTML = '\t\t\t\t<form action="" class="border-radius-btm-10px">'+
                        '\n\t\t\t\t\t<div class="choose-file">'+
                            '\n\t\t\t\t\t\t<label>'+ label +'</label>'+
                            '\n\t\t\t\t\t\t<input type="file" name="mainFile" id="mainFile" multiple="multiple"/>'+
                        '\n\t\t\t\t\t</div>'+
                    '\n\t\t\t\t</form>';
                    document.getElementById("mainFile").addEventListener("change", App.FileIO._Events.onFileLoaded);
                },
                Thumbnails: function(label){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    App.HTML.Thumnails.footer.innerHTML = '\t\t\t\t<form action="" class="border-radius-btm-10px">'+
                        '\n\t\t\t\t\t<div class="choose-file">'+
                            '\n\t\t\t\t\t<label>'+ label +'</label>'+
                            '\n\t\t\t\t\t<input type="file" name="ThumbnailFile" id="thumbnailsFile" multiple="multiple"/>'+
                        '\n\t\t\t\t\t</div>'+
                    '\n\t\t\t\t\t</form>';
                    document.getElementById("thumbnailsFile").addEventListener("change", App.FileIO._Events.onFileLoaded);
                },
                Div: function(div, label, name){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    div.innerHTML = '\t\t\t\t<form action="" class="border-radius-btm-10px">'+
                        '\n\t\t\t\t\t<div class="choose-file">'+
                            '\n\t\t\t\t\t<label>'+ label +'</label>'+
                            '\n\t\t\t\t\t<input type="file" name="'+ name +'" id="'+ name +'" multiple="multiple"/>'+
                        '\n\t\t\t\t\t</div>'+
                    '\n\t\t\t\t\t</form>';
                    
                    document.getElementById(name).addEventListener("change", App.FileIO._Events.onFileLoaded);
                }
            },
            Dropzone: {
                App: function(){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                
                    document.body.addEventListener("drop", App.FileIO._Events.onFileLoaded);
                    document.body.addEventListener("dragover", App.FileIO._Events.onFileDragOver);
                },
                Div: function(div){
                    var bHaveFileAPI = (window.File && window.FileReader);
    
                    if(!bHaveFileAPI){
                        os.windows.Create.ErrorWindow("File IO Not Supported", "The current browser does not support HTML5 File IO");
                        return;
                    }
                    
                    div.addEventListener("drop", App.FileIO._Events.onFileLoaded);
                    div.addEventListener("dragover", App.FileIO._Events.onFileDragOver);
                }
            }
        },
        Get: {
            Extension: function(file){
                return file.name.substr(file.name.lastIndexOf(".") + 1).toUpperCase();
            }
        },
        Read: {
            Text: function(file, callback, scope){
                var reader = new FileReader();
    
                reader.onload = function(e){
                    scope ? callback.apply(scope, [e.target.results]) : callback(e.target.result) ;
                }
                
                reader.readAsText(file);
            },
            Image: function(file, callback, scope){
                var reader = new FileReader();
                
                reader.onload = function(e){
                    img = new Image();
                    img.src = e.target.result;
                    scope ? callback.apply(scope, [img]) : callback(img) ;
                }
                
                reader.readAsDataURL(file);
            },
            DataURL: function(file, callback, scope){
                var reader = new FileReader();
                
                reader.onload = function(e){
                    scope ? callback.apply(scope, [e.target.result]) : callback(e.target.result) ;
                }
                
                reader.readAsDataURL(file);
            },
            ObjectURL: function(file){
                if ( window.webkitURL ) {
                    return window.webkitURL.createObjectURL( file );
                }
                else if ( window.URL && window.URL.createObjectURL ) {
                    return window.URL.createObjectURL( file );
                }
                else {
                    return null;
                }
            }
            
        },
        _Events: {
            onFileLoaded: function(e){
                e.stopPropagation();
                e.preventDefault();
                
                var files = e.target.files ? e.target.files : e.dataTransfer.files;
                var totalBytes = 0;
                
                for(var i = 0; i < files.length; i++){
                    var fileInfo = "File Name: " + files[i].name + "; Size: " + files[i].size + "; Type: " + files[i].type ;
                    totalBytes += files[i].size;
                    os.console.AppendComment(fileInfo);
                }
                
                os.console.Comment( "\nTotal of " + files.length + " files, " + totalBytes + " bytes.");
                
                App.Events.onFileLoaded(files);
            },
            onFileDragOver: function(e){
                e.stopPropagation();
                e.preventDefault();
            }
        }
    },
    Window: {
        Create: function(sTitle, sType, bDocked){
            var win = os.windows.WindowsManager.Create.Window(sTitle, sType);
            
            win.Hide.toolbar();
            win.Hide.menubar();
            win.elements.content.html().style.overflow = "hidden";
            win.Set.statusbarText("");
            win.Display.window();
            win.elements.titlebar.buttons.close.html().onclick = function(e){
                App.Window.Dock(os.windows.WindowsManager.Windows.get(e.target.id));    
            }
            win.onDock = function(){};
            win.onUndock = function(){};
            
            if(bDocked){App.Window.Dock(win);}
            
            return win;
        },
        Dock: function(win){

            win.Set.position(0,0);
            win.Set.width(358);
            win.Set.height(228);
            win.Hide.menubar();
            win.Hide.titlebar();
            win.Hide.statusbar();
            if(win.theme.name == "PC"){
                win.elements.window.Class.Remove("pcWindow ");
                win.elements.window.Class.Add("pcWindow-NoShadow");
            }
            else{
                win.elements.window.Class.Remove("macWindow ");
                win.elements.window.Class.Add("macWindow-NoShadow");
            }
            
            document.body.removeChild(win.elements.window.html());
            App.HTML.Properties.window.appendChild(win.elements.window.html());
            
            var idString = win.Get.id();
            idString = idString.split(".");
            
            var opt = document.createElement("option");
            opt.value = idString[idString.length - 1];
            opt.innerHTML = win.Get.title();
            
            App.HTML.Properties.dropDown.add(opt);
            
            App.HTML.Properties.dropDown.selectedIndex = opt.index;
            if(win.onDock){win.onDock();}
            
        },
        UnDock: function(e){
            if(App.HTML.Properties.dropDown.length > 0){
                var win = win = os.windows.WindowsManager.Windows.get("jahova.window.id." + App.HTML.Properties.dropDown[App.HTML.Properties.dropDown.selectedIndex].value);
        
                win.Set.position(100,100);
                win.Set.width(358);
                win.Set.height(228);
                win.Display.titlebar();
                win.Display.statusbar();
                win.elements.window.html().style.position = "absolute";
                if(win.theme.name == "PC"){
                    win.elements.window.Class.Remove("pcWindow-NoShadow ");
                    win.elements.window.Class.Add("pcWindow");
                }
                else{
                    win.elements.window.Class.Remove("macWindow-NoShadow ");
                    win.elements.window.Class.Add("macWindow");
                }
                win.MakeActive();
                if(win.onUndock){win.onUndock();}
                
                App.HTML.Properties.window.removeChild(win.elements.window.html());
                document.body.appendChild(win.elements.window.html());
            
                App.HTML.Properties.dropDown.remove(App.HTML.Properties.dropDown[App.HTML.Properties.dropDown.selectedIndex]);
                
                if(App.HTML.Properties.dropDown.length > 0){
                    win = os.windows.WindowsManager.Windows.get("jahova.window.id." + App.HTML.Properties.dropDown[App.HTML.Properties.dropDown.selectedIndex].value);
                    win.MakeActive();
                }
                
                
            }
        }
    },
    Events: {
        onDropdown: function(win){
            
        },
        onFullscreen: function(){
            
        },
        onWindowed: function(){
            
        },
        onFileLoaded: function(files){
            
        }
    },
    Init: function(){
    
    }
}


App.Init = function(){
    psl = playstylelabs.Instance();
    
    App.SS =  psl.Create.SpriteSheet("puppy");
    App.SS.width = 2926;
    App.SS.height = 1601;
    App.SS.cellWidth = 298;
    App.SS.cellHeight = 265;
    App.SS.rows = 6;
    App.SS.columns = 10;
    App.SS.path = "../scripts/Applications/puppySS.png";
    App.SS.BuildClass();
    
    App.Animations = {};
    App.Animations.Jump = psl.Create.Animation("jump", 10, 3, 0, App.SS, 90);
    
    App.Sprite = psl.Create.Sprite("puppy");
    App.Sprite.graphic.AppendTo(document.body)
    
    App.Sprite.Animation.Load(App.Animations.Jump);
    
    App.Sprite.Animation.SetActive("jump");
    
    var lastUpdate = 0;
    var time;
    
    App.Update = function(){
        if(App.Sprite.Animation.Current.animation){
            time = psl.Get.Time();
            if((time - lastUpdate) > App.Sprite.Animation.Current.animation.speed){
                lastUpdate = time;
                App.Sprite.Animation.NextFrame();
            }
        }
        
        window.requestAnimFrame(App.Update);
    }
    
    App.Update();
   
}














