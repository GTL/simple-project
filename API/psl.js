//
// Playstyle Labs Module
//
var CSprite = function(sName){
    var self = this;
    this.name = sName;
    this.graphic = pInstance.Create.HTML("div");
    this.graphic.html.id = sName;
    this.graphic.html.style.position = "absolute";
    this.Position ={
        x: 0,
        y: 0,
        z: 0
    }
    this.Rotation ={
        x: 0,
        y: 0,
        z: 0
    }
    this.Move = {
        Up: function(iDist){
            self.Position.y -= iDist;
            self.graphic.html.style.top = (self.Position.y).toFixed(0) + "px";
        },
        Down: function(iDist){
            self.Position.y += iDist;
            self.graphic.html.style.top = (self.Position.y).toFixed(0) + "px";
        },
        Left: function(iDist){
            self.Position.x -= iDist;
            self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
        },
        Right: function(iDist){
            self.Position.x += iDist;
            self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
        },
        ToPosition: function(iX, iY){
            self.Position.x = iX;
            self.Position.y = iY;
            
            if(self.graphic.html.style.webkitTransform != undefined)
                self.graphic.html.style.webkitTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
            else
                self.graphic.html.style.MozTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
            //self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
            //self.graphic.html.style.top  = (self.Position.y).toFixed(0) + "px";
        }
    }
    
    this.Rotate = function(deg){
        self.graphic.html.style.webkitTransform = "rotate("+ deg +"deg)";
    }
    
    this.Spin = {
        X: function(deg){
            
        },
        Y: function(deg){
            
        }
    }
    
    
    this.Animation = {
        list: pInstance.Create.Map(),
        Current: {
            animation: null,
            frames: 0,
            index: 0
        },
        loop: false,
        lastUpdate: 0,
        NextFrame: function(){//Transtion to next animation frame
            self.Animation.lastUpdate = pInstance.Get.Time();
            
            self.Animation.Current.index = self.Animation.Current.index == self.Animation.Current.animation.numOfFrames - 1 ? 0  : self.Animation.Current.index + 1;
            self.Animation.Current.frame = self.Animation.Current.animation.frames[self.Animation.Current.index];

            self.graphic.html.style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
        },
        Update: function(){   //Transition to Next Frame, if ready
            
        },
        Start: function(){    //Auto Animate
            
        },
        Pause: function(){    //Pause Animation 
            
        },
        Reset: function(){    //Reset Animation to First Frame
            self.Animation.lastUpdate = new Date().getDate();
        
            self.Animation.Current.index = 0;
            self.Animation.Current.frame = self.Animation.Current.animation.frames[0];
        },
        Load: function(oAnimation){     //Load New Animation into Sprite Object
            self.Animation.list.put(oAnimation.name, oAnimation);
        },
        SetActive: function(sName){//Set Animation
            //Remove Current Animation Class
            if(self.Animation.Current.animation)
                self.graphic.Remove.Class(self.Animation.Current.animation.sheet.GetClass() + " ");
            
            //Set new animation object
            self.Animation.Current.animation = self.Animation.list.get(sName);
            
            //Add animation class to html
            self.graphic.Append.Class(self.Animation.Current.animation.sheet.GetClass());
            
            //Update frame on graphic
            self.Animation.SetFrame(0);
        },
        SetFrame: function(iFrameNumber){ //Set To Specific Frame in Current Animation
            self.Animation.lastUpdate = new Date().getDate();
        
            self.Animation.Current.index = iFrameNumber;
            self.Animation.Current.frame = self.Animation.Current.animation.frames[iFrameNumber];

            self.graphic.html.style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
        }
        
    }
}

var CSpriteSheet = function(sName){
    var self = this;
    this.width = 0;
    this.height = 0;
    this.cellWidth = 0;
    this.cellHeight = 0;
    this.rows = 0;
    this.columns = 0;
    this.path = "";
    this.id = 0;
    this.name = sName;
    var className = ""
    this.BuildClass = function(sClassName){
        className = sClassName ? sClassName : self.name;

        var style = "position: absolute; ";
        style += "overflow: hidden;";
        style += "background: url('" + this.path + "'); ";
        style += "width: " + this.cellWidth +"px;";
        style += "height: " + this.cellHeight + "px;";
        
        pInstance.Create.CSSClass("."+className, style);
    }
    this.GetClass = function(){
        return className;
    }
}

var CAnimation = function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed){
    var self = this;
    this.name = sName;
    this.sheet = oSpriteSheet;
    this.numOfFrames = iNumOfFrames;
    this.StartFrame = null;
    this.StopFrame  = null;
    this.frames = [];
    this.speed =  speed ? speed : 0;
    //Set Stop Column, Row and index
    
    //Populate frames array
    
     {
        var _frame = 0;
        
        //need to have initial starting point, will be reset to 0 afterwards
        var _iStartColumn = iStartColumn;
        
        // runs through rows (y) and columns (x) and adds them to the frames array
        for(var y = iStartRow; y < oSpriteSheet.rows && _frame < iNumOfFrames; y++){
            for (var x = _iStartColumn; x < oSpriteSheet.columns && _frame < iNumOfFrames; x++,_frame++){
                var fr = new CAnimationFrame(x*oSpriteSheet.cellWidth,y*oSpriteSheet.cellHeight,oSpriteSheet.cellWidth,oSpriteSheet.cellHeight);
                fr.frame = _frame;
                fr.row = y;
                fr.column = x;
                self.frames.push(fr);
            }
            // resetting starting column (x) to 0, so it goes through all following columns
            _iStartColumn = 0;

        }
    }
}

var CAnimationFrame = function(iX, iY, iWidth, iHeight){
    this.x = iX;
    this.y = iY;
    this.width = iWidth;
    this.height = iHeight;
    this.frame = 0;         //Sprite Sheet Frame Number
    this.row = 0;
    this.column = 0;
}


//  Finite State Machine
var CFiniteStateMachine = function(cEntity){
    var owner = cEntity;
    var _CurrentState = new CState();
    var self = this;
    var _Enter = function(oMessage){
        _CurrentState.Enter(owner, oMessage);
    }
    
    var _Exit = function(oMessage){
        _CurrentState.Exit(owner, oMessage);
    }
    
    var _Execute = function(oMessage){
        _CurrentState.Execute(owner, oMessage);
    }
    
    //Control Methods
    this.Transition = function(sName, oMessage){
        //Call Exit for Current State
        _Exit(oMessage);
        
        //Get New State to Transition Too
        _CurrentState = _States.get(sName);
        
        //Call New States Setup and Exectue Methods
        _Enter(oMessage);
        _Execute(oMessage);
    }
    this.SetState = function(sName){
        _CurrentState = _States.get(sName);
    }
    this.Update = function(oMessage){
        _Execute(oMessage);
    }
    this.GetState = function(){
        return _CurrentState.GetName();
    }
}

// States for FSM
var CState = function(sName){
    var _name = sName;
    var self = this;
    
    this.Enter = function(cEntity, oMessage){
        //Override this Method
    }
    
    this.Exit = function(cEntity, oMessage){
        //Override this Method
    }
    
    this.Execute = function(cEntity, oMessage){
        //Override this Method
    }
    
    this.GetName = function(){
        return _name;
    }

}
       
var psl = {
    Get: {
        Sprite: function(sName){
            return Sprites.get(sName);
        },
        SpriteSheet: function(sName){
            return SpriteSheets.get(sName)
        },
        Animation: function(sName){
            return Animations.get(sName);
        },
        Time: function(){
            return Date.now();
        },
        State: function(sName){
            return _States.get(sName);  
        },
        Name: function(){
            return NAME;
        },
        Version: function(){
            return VERSION;
        },
        Path: function(){
            return PATH;
        }
    },
    Remove: {
        Sprite: function(sName){
            return Sprites.drop(sName);
        },
        SpriteSheet: function(sName){
            return SpriteSheets.drop(sName)
        },
        Animation: function(sName){
            return Animations.drop(sName);
        }
    },
    
    Create: {
        FSM: function(cEntity){
            return new CFiniteStateMachine(cEntity);
            
        },
        State: function(sName){
            var state = new CState(sName);
            _States.put(sName, state);
            return state;
        },
        Sprite: function(sName){
            var sprite = new CSprite(sName);
            Sprites.put(sName, sprite);
            
            return  sprite;
        },
        SpriteSheet: function(sName){
            var sheet = new CSpriteSheet(sName);
            SpriteSheets.put(sName, sheet);
            
            return sheet;   
        },
        Animation: function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed){
            var ani = new CAnimation(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed);
            Animations.put(sName, ani);
            
            return ani;
        },
        AnimationFrame: function(iX, iY, iWidth, iHeight){
            return new CAnimationFrame(iX, iY, iWidth, iHeight);
        },
        CSSClass: function(sClass, sStyle) {
            if (!document.styleSheets) {
                return;
            }
        
            if (document.getElementsByTagName("head").length == 0) {
                return;
            }
        
            var stylesheet;
            var mediaType;
            if (document.styleSheets.length > 0) {
                for (i = 0; i < document.styleSheets.length; i++) {
                    if (document.styleSheets[i].disabled) {
                        continue;
                    }
                    var media = document.styleSheets[i].media;
                    mediaType = typeof media;
        
                    if (mediaType == "string") {
                        if (media == "" || (media.indexOf("screen") != -1)) {
                            styleSheet = document.styleSheets[i];
                        }
                    } else if (mediaType == "object") {
                        if (media.mediaText == "" || (media.mediaText.indexOf("screen") != -1)) {
                            styleSheet = document.styleSheets[i];
                        }
                    }
        
                    if (typeof styleSheet != "undefined") {
                        break;
                    }
                }
            }
        
            if (typeof styleSheet == "undefined") {
                var styleSheetElement = document.createElement("style");
                styleSheetElement.type = "text/css";
        
                document.getElementsByTagName("head")[0].appendChild(styleSheetElement);
        
                for (i = 0; i < document.styleSheets.length; i++) {
                    if (document.styleSheets[i].disabled) {
                        continue;
                    }
                    styleSheet = document.styleSheets[i];
                }
        
                var media = styleSheet.media;
                mediaType = typeof media;
            }
        
            if (mediaType == "string") {
                for (i = 0; i < styleSheet.rules.length; i++) {
                    if (styleSheet.rules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                        styleSheet.rules[i].style.cssText = sStyle;
                        return;
                    }
                }
        
                styleSheet.addRule(sClass, sStyle);
            }
            else if (mediaType == "object") {
                for (i = 0; i < styleSheet.cssRules.length; i++) {
                    if (styleSheet.cssRules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                        styleSheet.cssRules[i].style.cssText = sStyle;
                        return;
                    }
                }
        
                styleSheet.insertRule(sClass + "{" + sStyle + "}", 0);
            }
        }
        
    }
        
    }
}